var express = require('express')
module.exports = {
  devServer: {
    open: true,
    host: '0.0.0.0',
    port: 8082,
    before(app, server) {
      app.use(express.static('node_modules'))
    },
    after(app, server) {
      app.use(express.static('public'))
    }
  },
  publicPath: './',
  outputDir: 'dist',
  lintOnSave: true,
  chainWebpack: (config) => {
    config.module
        .rule('')
        .test(/mxClient\.js$/)
        .use('exports-loader')
        .loader('exports-loader?mxClient,mxGraphModel,mxActor,mxShape,mxEventObject,mxGraph,mxPrintPreview,mxEventSource,mxRectangle,mxVertexHandler,mxMouseEvent,mxGraphView,mxImage,mxGeometry,mxRubberband,mxKeyHandler,mxDragSource,mxGraphModel,mxEvent,mxUtils,mxWindow,mxEvent,mxCodec,mxCell,mxConstants,mxPoint,mxGraphHandler,mxCylinder,mxCellRenderer,mxEvent,mxUndoManager')
        .end();
    // config.resolve.alias
    //     .set('@', resolve('src'))
    //     .set('@assets', resolve('src/assets'));
    // 按这种格式.set('', resolve('')) 自己添加
  }
};