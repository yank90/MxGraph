import {
  mxConstants as MxConstants
} from 'mxgraph/javascript/mxClient'

const outputIcon = './icon/mouse.png'
const inputIcon = './icon/mouse.png'


// image;html=1;labelBackgroundColor=#ffffff;image=stencils/clipart/Laptop_128x128.png

export const toolbarItems = [
  {
    icon: inputIcon,
    type:'startEvent',
    title: '开始',
    width: 50,
    height: 50,
    attribute:[
      {
        key:'定义属性4',
        value:'默认值'
      },
      {
        key:'定义属性5',
        value:'默认值'
      },
      {
        key:'定义属性6',
        value:'默认值'
      }
    ],
    style: {
      fillColor: 'transparent', // 填充色
      strokeColor: '#000000', // 线条颜色
      strokeWidth: '1', // 线条粗细
      rounded:'1',
      shape: MxConstants.SHAPE_IMAGE, // 形状
      align: MxConstants.ALIGN_CENTER, // 水平方向对其方式
      verticalAlign: MxConstants.ALIGN_BOTTOM, // 垂直方向对其方式
      imageAlign: MxConstants.ALIGN_CENTER, // 图形水平方向对其方式
      imageVerticalAlign: MxConstants.ALIGN_TOP, // 图形方向对其方式
      image: inputIcon, // 图形
      spacingBottom:-25,
      imageWidth:'48',
      imageHeight:'48'
    }
  },
  {
    icon: inputIcon,
    title: '结束',
    type:'endEvent',
    width: 50,
    height: 50,
    attribute:[
      {
        key:'定义属性4',
        value:'默认值'
      },
      {
        key:'定义属性5',
        value:'默认值'
      },
      {
        key:'定义属性6',
        value:'默认值'
      }
    ],
    style: {
      fillColor: 'transparent', // 填充色
      strokeColor: '#000000', // 线条颜色
      strokeWidth: '1', // 线条粗细
      rounded:'1',
      shape: MxConstants.SHAPE_IMAGE, // 形状
      align: MxConstants.ALIGN_CENTER, // 水平方向对其方式
      verticalAlign: MxConstants.ALIGN_BOTTOM, // 垂直方向对其方式
      imageAlign: MxConstants.ALIGN_CENTER, // 图形水平方向对其方式
      imageVerticalAlign: MxConstants.ALIGN_TOP, // 图形方向对其方式
      image: inputIcon, // 图形
      spacingBottom:-25,
      imageWidth:'48',
      imageHeight:'48'
    }
  },
  {
    icon: inputIcon,
    type:'type3',
    title: '结束事件4',
    width: 100,
    height: 50,
    attribute:[
      {
        key:'定义属性4',
        value:'默认值'
      },
      {
        key:'定义属性5',
        value:'默认值'
      },
      {
        key:'定义属性6',
        value:'默认值'
      }
    ],
    style: {
      fillColor: 'transparent', // 填充色
      strokeColor: '#000000', // 线条颜色
      strokeWidth: '1', // 线条粗细
      rounded:'1',
      shape: MxConstants.SHAPE_LABEL, // 形状
      align: MxConstants.ALIGN_CENTER, // 水平方向对其方式
      verticalAlign: MxConstants.ALIGN_BOTTOM, // 垂直方向对其方式
      imageAlign: MxConstants.ALIGN_CENTER, // 图形水平方向对其方式
      imageVerticalAlign: MxConstants.ALIGN_TOP, // 图形方向对其方式
      image: inputIcon // 图形
    }
  }
]
