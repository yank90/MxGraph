/**
 * 将平级对象列表转换为树形结构对象列表。
 */
export function convertToTree(data, idPropertyName = 'id', parentIdPropertyName = 'parentId', childrenPropertyName = 'children') {
  const result = []
  function buildChildren(item) {
    const id = item[idPropertyName]
    const children = data.filter(item2 => item2.hasOwnProperty(parentIdPropertyName) && item2[parentIdPropertyName] === id)
    if (children.length > 0) {
      children.forEach(buildChildren)
      item[childrenPropertyName] = children
    }
  }
  for (const item of data) {
    if (!item.hasOwnProperty(parentIdPropertyName) || !item[parentIdPropertyName] || item[parentIdPropertyName] === null) {
      buildChildren(item)
      result.push(item)
    }
  }
  return result
}

/**
 * 将平级对象列表转换为树形结构对象列表。
 */
export function convertToCascader(data, idPropertyName = 'id', parentIdPropertyName = 'parentId', childrenPropertyName = 'children') {
  const result = []
  function buildChildren(item) {
    const id = item[idPropertyName]
    const children = data.filter(item2 => item2.hasOwnProperty(parentIdPropertyName) && item2[parentIdPropertyName] === id)
    if (children.length > 0) {
      children.forEach(buildChildren)
      item[childrenPropertyName] = children
    }
  }
  for (const item of data) {
    if (!item.hasOwnProperty(parentIdPropertyName) || !item[parentIdPropertyName] || item[parentIdPropertyName] === null) {
      buildChildren(item)
      result.push({value: item[idPropertyName], label: item[idPropertyName]})
    }
  }
  return result
}
