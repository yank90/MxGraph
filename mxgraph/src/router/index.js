import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import index from '../views/index.vue'
import index2 from '../views/index2.vue'
import  WorkflowDesignerTest from '../views/WorkflowDesignerTest.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
      path: '/index',
      name: 'Index',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: index
  },
    {
      path: '/index2',
      name: 'Index2',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: index2
    },
    {
      path: '/workflowDesignerTest',
      name: 'WorkflowDesignerTest',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: WorkflowDesignerTest
    }
]

const router = new VueRouter({
  routes
})

export default router
